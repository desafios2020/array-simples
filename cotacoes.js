
var cotacoes = [];

//Adiciona elementos no array
function adicionar(input){
    cotacoes.push(input.value);
    console.log(cotacoes);
}

//Remove qualquer elemento do array
function remover(input){
    let elemento;
    for (let index = 0; index < cotacoes.length; index++) {
        elemento = cotacoes[index];
        if(elemento == input.value){
            cotacoes.splice(index, 1);
        }
    }
    console.log(cotacoes);
}

// Altera o título de uma das cotações
function alterarTitulo(inputTituloAtual, inputTituloNovo){
    let elemento;
    for (let index = 0; index < cotacoes.length; index++) {
        elemento = cotacoes[index];
        if(elemento == inputTituloAtual.value){
            cotacoes[index] = inputTituloNovo.value;
        }
    }
    console.log(cotacoes);
}

